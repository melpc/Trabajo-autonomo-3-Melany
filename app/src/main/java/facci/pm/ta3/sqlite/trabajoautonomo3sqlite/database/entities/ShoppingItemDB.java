package facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.widget.Toast;

import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.MainActivity;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.helper.ShoppingElementHelper;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.model.ShoppingItem;
import java.util.ArrayList;

public class ShoppingItemDB {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private ShoppingElementHelper dbHelper;

    public ShoppingItemDB(Context context) {
        // Create new helper
        dbHelper = new ShoppingElementHelper(context);
    }

    /* Inner class that defines the table contents */
    public static abstract class ShoppingElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }


    public void insertElement(String productName) {

        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos

        SQLiteDatabase database = dbHelper.getWritableDatabase(); // Se obtiene la referncia de nuestra base de datos
        ContentValues values = new ContentValues(); // se almacena de manera temporal nuestros datos relacionándolos a la columna a la cual pertenecen
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE, productName); // m
        database.insert(ShoppingElementEntry.TABLE_NAME, null, values); //Indica a la base de datos que deseamos
                                                                                        // insertar un nuevo elemento en la tabla

    }


    public ArrayList<ShoppingItem> getAllItems() {

        ArrayList<ShoppingItem> shoppingItems = new ArrayList<>();

        String[] allColumns = { ShoppingElementEntry._ID,
            ShoppingElementEntry.COLUMN_NAME_TITLE};

        Cursor cursor = dbHelper.getReadableDatabase().query(
            ShoppingElementEntry.TABLE_NAME,    // The table to query
            allColumns,                         // The columns to return
            null,                               // The columns for the WHERE clause
            null,                               // The values for the WHERE clause
            null,                               // don't group the rows
            null,                               // don't filter by row groups
            null                                // The sort order
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ShoppingItem shoppingItem = new ShoppingItem(getItemId(cursor), getItemName(cursor));
            shoppingItems.add(shoppingItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return shoppingItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(ShoppingElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(ShoppingElementEntry.COLUMN_NAME_TITLE));
    }


    public void clearAllItems() {
        //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos

        SQLiteDatabase database = dbHelper.getWritableDatabase(); // Se obtiene la referncia de nuestra base de datos
        database.delete(ShoppingElementEntry.TABLE_NAME, null, null); // elimina el registro en este caso
                                                                                            // la tabla que queremos eliminar
    }

    public void updateItem(ShoppingItem shoppingItem) {

        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos
        SQLiteDatabase database = dbHelper.getWritableDatabase(); // Se obtiene la referncia de nuestra base de datos
        ContentValues values = new ContentValues(); // almacena nuestros valores en sus respectivas columnas
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE, shoppingItem.getName()); // Colocamos el campo al cual queremos actualizar
        String selection = ShoppingElementEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(shoppingItem.getId())};
        database.update(ShoppingElementEntry.TABLE_NAME, values, selection, selectionArgs);//establece que tabla queremos utilizar, los valores a cambiar
                                                                                            // y la condicional para realizar el cambio
    }


    public void deleteItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ELIMINAR un Item de la Base de datos

        SQLiteDatabase database = dbHelper.getWritableDatabase(); // Se obtiene la referncia de nuestra base de datos
        String selection = ShoppingElementEntry.COLUMN_NAME_TITLE + " LIKE ?"; //
        String[] selectionArgs = {shoppingItem.getName()};
        database.delete(ShoppingElementEntry.TABLE_NAME,selection,selectionArgs); //Se coloca la tabla de la que queremos eliminar, se manda el campo
                                                                        // de consulta y los paramatros de consulta para eliminar dicho Item con ese id


    }
}
